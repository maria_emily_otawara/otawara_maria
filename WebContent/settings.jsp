<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>${loginUser.account}の設定</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

            <c:if test="${ not empty errorPosts }">
                <div class="errorPosts">
                    <ul>
                        <c:forEach items="${errorPosts}" var="post">
                            <li><c:out value="${post}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorPosts" scope="session"/>
            </c:if>

            <form action="settings" method="post"><br />

                <label for="account">アカウント名</label>
                <input name="account" value="${editUser.account}" /><br />

                <label for="password">パスワード</label>
                <input name="password" type="password" id="password"/> <br />

                <input name="id" value="${editUser.id}" id="id" type="hidden"/>
                <label for="name">名前</label>
                <input name="name" value="${editUser.name}" id="name"/>（名前はあなたの公開プロフィールに表示されます）<br />

                <label for="branch_id">支店名</label>
                <input name="branch_id" value="${editUser.branch_id}" id="branch_id"/> <br />

                <label for="department">部署・役所</label>
                <input name="department" value="${editUser.department}" id="department"/> <br />

                <input type="submit" value="登録" /> <br />
                <a href="./">戻る</a>
            </form>
            <div class="copyright"> Copyright(c)Maria Emily</div>
        </div>
    </body>
</html>