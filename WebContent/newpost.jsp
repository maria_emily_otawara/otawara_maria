<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
</head>
<body>
	<div class="main-contents">
            <div class="header">
                <a href="top">ホーム</a>
                <a href="logout">ログアウト</a>
            </div>
            <div class="copyright"> Copyright(c)Maria Emily</div>
    </div>
</body>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            投稿ID<br />
            <input name="postid"/>
            <br />
        </form>
    </c:if>
</div>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            登録者<br />
            <input name="creator"/>
            <br />
        </form>
    </c:if>
</div>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            件名<br />
            <input name="title"/>
            <br />
        </form>
    </c:if>
</div>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            カテゴリー<br />
            <input name="category"/>
            <br />
        </form>
    </c:if>
</div>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            本文<br />
            <textarea name="body" cols="100" rows="5" class="post-box"></textarea>
            <br />
            <input type="submit" value="投稿">（1000文字まで）
        </form>
    </c:if>
</div>
<div class="form-area">
    <c:if test="${ ShowPost }">
        <form action="newpost" method="post">
            登録日時<br />
            <div class="date"><fmt:formatDate value="${post.created_date}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
            <br />
        </form>
    </c:if>
</div>
</html>