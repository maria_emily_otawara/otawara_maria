package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserPost;
import exception.SQLRuntimeException;

public class UserPostDao {

    public List<UserPost> getUserPosts(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("posts.post_id as post_id, ");
            sql.append("users.name as name, ");
            sql.append("posts.title as title, ");
            sql.append("posts.body as body, ");
            sql.append("posts.category as category, ");
            sql.append("posts.created_date as created_date, ");
            sql.append("posts.creator as creator ");
            sql.append("FROM posts ");
            sql.append("INNER JOIN users ");
            sql.append("ON posts.creator = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserPost> ret = toUserPostList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserPost> toUserPostList(ResultSet rs)
            throws SQLException {

        List<UserPost> ret = new ArrayList<UserPost>();
        try {
            while (rs.next()) {
            	int post_id = rs.getInt("post_id");
            	String name = rs.getString("name");
                String title = rs.getString("title");
                String body = rs.getString("body");
                String category = rs.getString("category");
                Date created_date = rs.getDate("created_date");
                int creator = rs.getInt("creator");

                UserPost post = new UserPost();
                post.setPost_Id(post_id);
                post.setName(name);
                post.setTitle(title);
                post.setBody(body);
                post.setCategory(category);
                post.setCreated_Date(created_date);
                post.setCreator(creator);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}