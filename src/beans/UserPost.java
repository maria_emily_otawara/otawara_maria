package beans;

import java.io.Serializable;
import java.sql.Date;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int post_id;
    private String name;
    private String title;
    private String body;
    private String category;
    private Date created_date;
    private int creator;

	public int getPost_Id() {
		return post_id;
	}
	public void setPost_Id(int post_id) {
		this.post_id = post_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getCreated_Date() {
		return created_date;
	}
	public void setCreated_Date(Date created_date) {
		this.created_date = created_date;
	}
	public int getCreator() {
		return creator;
	}
	public void setCreator(int creator) {
		this.creator = creator;
	}
}
