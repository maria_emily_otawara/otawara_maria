package beans;

public class User {
	private int id;
    private String account;
    private String password;
    private String name;
    private String branch_id;
    private String department;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranch_Id() {
		return branch_id;
	}
	public void setBranch_Id(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public void SetName(String parameter) {
		// TODO 自動生成されたメソッド・スタブ

	}
}
