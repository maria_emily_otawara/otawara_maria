package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet(urlPatterns = { "/newpost" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

		 request.getRequestDispatcher("/newpost.jsp").forward(request, response);

	}
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

			HttpSession session = request.getSession();

			List<String> errormessage = new ArrayList<String>();

			if (isValid(request, errormessage) == true) {

	private boolean isValid(HttpServletRequest request, List<String> errormessage) {


        String creator = request.getParameter("creator");
        String title = request.getParameter("title");
        String body = request.getParameter("body");

        if (creator.isEmpty() == true) {
            errormessage.add("本文を入力してください");
        }

        if (body.isEmpty() == true) {
            errormessage.add("本文を入力してください");
        }
        if (30 < title.length()) {
            errormessage.add("30文字以下で入力してください");
        }
        if (1000 < body.length()) {
            errormessage.add("1000文字以下で入力してください");
        }

        //エラー判定
        if (errormessage.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}
