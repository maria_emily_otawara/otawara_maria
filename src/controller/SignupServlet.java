
package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> posts = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, posts) == true) {

        	User user = new User();
            user.setAccount(request.getParameter("account"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranch_Id(request.getParameter("branch_id"));
            user.setDepartment(request.getParameter("department"));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorPosts", posts);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> posts) {
        String account = request.getParameter("account");
        String password = request.getParameter("password");

        if (StringUtils.isEmpty(account) == true) {
            posts.add("アカウント名を入力してください");
        }
        if (StringUtils.isEmpty(password) == true) {
            posts.add("パスワードを入力してください");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
        if (posts.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}